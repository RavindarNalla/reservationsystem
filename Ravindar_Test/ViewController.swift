//
//  ViewController.swift
//  Ravindar_Test
//
//  Created by Ravindar on 18/06/19.
//  Copyright © 2019 Ravindar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var txtFieldS: UITextField!
    @IBOutlet weak var txtFieldNValue: UITextField!
    
    @IBOutlet weak var lblNumberOfSeatsAvailable: UILabel!
    var arrTotalNumberOfSeats:NSMutableArray = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func btnAvailableSeatsCountClicked(_ sender: Any) {
        if (txtFieldS.text?.count)! > 1 && (txtFieldNValue.text?.count)! > 0 {
            checkForAvailableSeats()
        }
        else {
            lblNumberOfSeatsAvailable.text = "Please enter Seats String like '1A 2B 5K' and number of Rows(N)"
        }
    }
    
    func checkForAvailableSeats() {
        arrTotalNumberOfSeats.removeAllObjects()
        
        let NValue: Int = Int(txtFieldNValue.text!)!
        for i in 1...NValue {
            arrTotalNumberOfSeats.add(NSString (format: "%dA%dB%dC", i,i,i))
            arrTotalNumberOfSeats.add(NSString (format: "%dD%dE%dF", i,i,i))
            arrTotalNumberOfSeats.add(NSString (format: "%dE%dF%dG", i,i,i))
            arrTotalNumberOfSeats.add(NSString (format: "%dH%dJ%dK", i,i,i))
        }
        
        print("Total number of seats --->",arrTotalNumberOfSeats.count)
        
        let strTemp:NSString = (txtFieldS!.text?.capitalized as NSString?)!
        var arrTemp:NSArray = strTemp.components(separatedBy: NSCharacterSet.whitespaces) as NSArray
        
        let resultPredicate : NSPredicate = NSPredicate(format: "SELF != %@", "")
        arrTemp =  arrTemp.filtered(using: resultPredicate) as NSArray
        
        for j in 0..<arrTemp.count {
            
            let str:NSString = arrTemp.object(at: j) as! NSString
            
            for k in 0..<arrTotalNumberOfSeats.count {
                
                let strSeat:NSString = arrTotalNumberOfSeats.object(at: k) as! NSString
                
                if strSeat.contains(str as String) {
                    
                    arrTotalNumberOfSeats.removeObject(at: k)
                    
                    if  k <= arrTotalNumberOfSeats.count {
                         let strSeatNext:NSString = arrTotalNumberOfSeats.object(at: k) as! NSString
                         if strSeatNext.contains(str as String) {
                            arrTotalNumberOfSeats.removeObject(at: k)
                        }
                    }
                    break
                }
            }
            
            var availableSeatsCount = arrTotalNumberOfSeats.count
            
            for m in 0..<arrTotalNumberOfSeats.count {
                
                let strSeatWithEF:NSString = arrTotalNumberOfSeats.object(at: m) as! NSString
                
                if  strSeatWithEF.contains("E") && strSeatWithEF.contains("F") {
                    
                    if m < (arrTotalNumberOfSeats.count-1) {
                        let strSeatWithSecondEF:NSString = arrTotalNumberOfSeats.object(at: m+1) as! NSString
                         if  strSeatWithSecondEF.contains("E") && strSeatWithSecondEF.contains("F") {
                            arrTotalNumberOfSeats.removeObject(at: m+1)
                            availableSeatsCount = availableSeatsCount-1
                         }
                    }
                    break
                    
                }
            }
            print("Number of available seats for a family of 3 members to sit next to each other. --->",arrTotalNumberOfSeats.count,"And the seats are: ",arrTotalNumberOfSeats)
            lblNumberOfSeatsAvailable.text = "Number of available seats for a family of 3 members to sit next to each other : \(arrTotalNumberOfSeats.count), \(availableSeatsCount)"
        }
        
    }
    
}
